//
//  FavoritesList.swift
//  Fonts
//
//  Created by Sergey Bavykin on 3/31/16.
//  Copyright © 2016 Sergey Bavykin. All rights reserved.
//

import Foundation
import UIKit

class FavouritesList {
    static let sharedFavoritesList = FavouritesList()
    private(set) var favorites: [String]
    
    private init() {
        let defaults = NSUserDefaults.standardUserDefaults()
        let storedFavourites = defaults.objectForKey("favorites") as? [String]
        
        favorites = storedFavourites != nil ? storedFavourites! : []
    }
    
    func addFavourite(fontName: String) {
        if !favorites.contains(fontName) {
            favorites.append(fontName)
            NSLog("\(favorites)")
            saveFavourites()
        }
    }
    
    func removeFavorite(fontName: String) {
        if let index = favorites.indexOf(fontName) {
            favorites.removeAtIndex(index)
            saveFavourites()
        }
    }
    
    func moveItem(fromIndex from: Int, toIndex to: Int) {
        let item = favorites[from]
        favorites.removeAtIndex(from)
        favorites.insert(item, atIndex: to)
        saveFavourites()
    }
    
    private func saveFavourites() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(favorites, forKey: "favorites")
        defaults.synchronize()
    }
}
